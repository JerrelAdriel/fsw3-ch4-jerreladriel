/*
 * Contoh kode untuk membaca query parameter,
 * Siapa tau relevan! :)
 * */

const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());

// Coba olah data ini hehe :)
console.log(params);

/*
 * Contoh penggunaan DOM di dalam class
 * */
const app = new App();
const btn_load = app.loadButton;
btn_load.addEventListener("click", function (loadbtn) {
  loadbtn.preventDefault();
  app.clear();
  app.load(app.penumpang.value).then(app.run);
});

