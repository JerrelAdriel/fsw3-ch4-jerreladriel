class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
    <div class="card" style="width: 18rem;">
    <img src="${this.image}" alt="${this.manufacture}" style="width:18rem; height: 18rem">
      <div class="card-body">
        <p>${this.type}</p>
        <p><b>${this.rentPerDay} / hari</b></p>
        <p>${this.description}</p>
        <p><img src="img/fi_users.png" alt="icon_capacity"/>  ${this.capacity} orang</p>
        <p><img src="img/fi_settings.png" alt="icon_transmission"/> ${this.transmission}</p>
        <p><img src="img/fi_calendar.png" alt="icon_Year"/> Tahun ${this.year}</p>
        <button class="btn btn-success btn-style" >Pilih Mobil</button>
      </div>
    </div>      
    
    `;
  }
}
