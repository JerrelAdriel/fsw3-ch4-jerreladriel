class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");


    this.tanggal = document.getElementById("tanggal");
    this.waktu = document.getElementById("waktu");
    this.penumpang = document.getElementById("penumpang");

  }

  // async init() {
  //   await this.load();
    // Register click listener
    // this.clearButton.onclick = this.clear;
    // this.loadButton.onclick = this.run;
    // this.buttonCari.onclick = this.run;
  // }

  run = () => {
    Car.list.forEach((car) => {
        const node = document.createElement("div");
        node.classList.add('col-lg-4')
        node.innerHTML = car.render();
        this.carContainerElement.appendChild(node);
    });
  };

  async load(penumpang) {
    this.tanggal = document.getElementById("tanggal").value;
    this.waktu = document.getElementById("waktu").value;

    const datetime = new Date(`${this.tanggal} ${this.waktu.substr(0, 5)}`);
    const beforeEpochTime = datetime.getTime();
    const cars = await Binar.listCars((item) => {
      const itemDate = new Date(item.availableAt);
      const date_filter = itemDate.getTime() < beforeEpochTime;
      return date_filter && item.capacity >= Number(penumpang);
    });
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
