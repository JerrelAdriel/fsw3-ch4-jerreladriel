const http = require('http');
const { PORT = 8000 } = process.env;

const fs = require('fs');
const path = require('path');
const { append } = require('express/lib/response');
const PUBLIC_DIRECTORY = path.join(__dirname, '../public/');
const mime = require('mime-types');
const url = require('url');

console.log(PUBLIC_DIRECTORY);

function onRequest(req, res) {
  let parseURL = url.parse(req.url, true);
  let fileName = parseURL.path.replace(/^\/+|\/+$/g, "");
  console.log(fileName, ' Ini FileName');
  switch(fileName) {
    case '':
      fileName = "index.html";
      break;
    case 'cars' :
      fileName = "cars.html";
      break;

  }
  let file = PUBLIC_DIRECTORY + fileName;
  console.log(file, "ini file");
  fs.readFile(file, function(err, data) {
    if (err) {
      res.writeHead(404);
      res.end("404");
    } else {
      res.setHeader("X-Content-Type-Options", "nosniff");
      let mimePath = mime.lookup(fileName);
      res.writeHead(200, {"Content-Type": mimePath})
      res.end(data)
    }
  })

}

const server = http.createServer(onRequest);

server.listen(PORT, 'localhost', () => {
  console.log("Server sudah berjalan, silahkan buka http://localhost:%d", PORT);
})